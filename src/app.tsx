import { createContext } from 'preact'
import { useCallback, useMemo, useReducer, useState } from 'preact/hooks'

type Item = Readonly<{
	id: string
	name: string
	age: number
	gender: 'm' | 'f'
}>

const createItem = (() => {
	let idx = 0
	return (): Item => {
		const id = `${++idx}`
		const name = `hoge${id}`
		const age = Math.floor(Math.random() * 10) + 10
		const gender: 'm' | 'f' = Math.random() < 0.5 ? 'm' : 'f'
		return {
			id,
			name,
			age,
			gender,
		}
	}
})()

const lines: readonly Item[] = Array.from({ length: 10 }, createItem)

const keys = ['id', 'name', 'age', 'gender'] as const

interface Ctx {
	texts: Record<string, string>
	setText: (key: string, set: (val: string) => string) => void
}

const ctx = createContext<Ctx | null>(null)

export const App = () => {
	const [mode, setMode] = useState(true)
	const [texts, setTexts] = useState({} as Record<string, string>)
	const setText = useCallback(
		(key: string, set: (val: string) => string) => {
			setTexts(ts => ({ ...ts, [key]: set(ts?.[key] || '') }))
		},
		[setTexts],
	)
	return (
		<main role="application" style={{ userSelect: 'none' }}>
			<ctx.Provider value={{ texts, setText }}>
				<label htmlFor="input">
					<table>
						<thead>
							<tr>
								{keys.map(k => (
									<th key={k}>{k}</th>
								))}
							</tr>
						</thead>
						<tbody>
							{lines.map(line => (
								<tr key={line.id}>
									{keys.map(k => (
										<td key={k}>{line[k]}</td>
									))}
								</tr>
							))}
						</tbody>
					</table>
					<p>
						<a
							onClick={() => {
								setMode(m => !m)
							}}
							tabIndex={-1}
						>
							toggle
						</a>
					</p>
					{mode && (
						<p>
							<input
								id="input"
								type="text"
								value={texts['input']}
								onChange={e =>
									setText(
										'input',
										() => (e.target as { value: string } | null)?.value || '',
									)
								}
							/>
						</p>
					)}
					{!mode &&
						Array.from({ length: 5 }, (_, id) => `${id + 1}`).map(id => (
							<p key={id}>
								<textarea
									id="input"
									value={texts[id]}
									onChange={e =>
										setText(
											id,
											() => (e.target as { value: string } | null)?.value || '',
										)
									}
								/>
							</p>
						))}
				</label>
			</ctx.Provider>
		</main>
	)
}
